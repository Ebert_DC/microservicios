// El lo primero que se ejecuta cunando inicialicemos un microcervicio
// Al llegar una consulta bull lo capta y lo procesara

const bull = require('bull');

const { redis } = require('../settings');
// Se crea un objeto para eliminar las repeticiones 
const options = { redis: { host: redis.host, port: redis.port } }

// Con esto queda el codigo listo con las varibles definidas para poder procesarlo
const queueCreate = bull("curso:Create", options );

const queueDelete = bull("curso:Delete", options );

const queueUpdate = bull("curso:Update", options );

const queueFindOne = bull("curso:FindOne", options );

const queueView = bull("curso:View", options );
	
module.exports = { queueCreate, queueDelete, queueUpdate, queueFindOne, queueView }