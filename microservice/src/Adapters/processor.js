// Alguien que adapta al mundo exterior
//Los adaptadores no pueden tener mas de un servicio
const Services = require('../Services');
const { InternalError } = require('../settings');

const { queueView, queueCreate, queueDelete, queueFindOne, queueUpdate } = require('./index');
// Lo extendemos para despues poder tener definir varios endpoint
async function View (job, done) {

	try {
		// Se destructutura la data para optener el ID
		const {  } = job.data;

		console.log(job.id);

		let { statusCode, data, message} = await Services.View({ });
		
		done(null, { statusCode, data: data.map(v => ({ ...v.toJSON(), ...{worker: job.id }})), message});
		

	} catch (error) {
		console.log({ step: 'adapter queueView', error: error.toString() });

		done(null, { statusCode: 500, message: InternalError });
	}

};

async function Create (job, done) {

	try {
		// Se destructutura la data para optener el ID
		const { age, color, name } = job.data;

		let { statusCode, data, message} = await Services.Create({ age, color, name});
		
		done(null, { statusCode, data, message});
		

	} catch (error) {
		console.log({ step: 'adapter queueCreate', error: error.toString() });

		done(null, { statusCode: 500, message: InternalError });
	}

};

async function Delete (job, done) {

	try {
		// Se destructutura la data para optener el ID
		const { id } = job.data;

		let { statusCode, data, message} = await Services.Delete({ id });
		
		done(null, { statusCode, data, message});
		

	} catch (error) {
		console.log({ step: 'adapter queueDelete', error: error.toString() });

		done(null, { statusCode: 500, message: InternalError });
	}

};

async function FindOne (job, done) {

	try {
		// Se destructutura la data para optener el ID
		const { name } = job.data;

		let { statusCode, data, message} = await Services.FindOne({ name });
		
		done(null, { statusCode, data, message});
		

	} catch (error) {
		console.log({ step: 'adapter queueFindOne', error: error.toString() });

		done(null, { statusCode: 500, message: InternalError });
	}

};

async function Update (job, done) {

	try {
		// Se destructutura la data para optener el ID
		const { age, color, id, name } = job.data;

		let { statusCode, data, message} = await Services.Update({ age, color, id, name });
		
		done(null, { statusCode, data, message});
		

	} catch (error) {
		console.log({ step: 'adapter queueUpdate', error: error.toString() });

		done(null, { statusCode: 500, message: InternalError });
	}

};

async function run() {
	try {
		
		console.log("Vamos a inicializar la Worker");

		queueView.process(View);
		queueCreate.process(Create);
		queueDelete.process(Delete);
		queueFindOne.process(FindOne);
		queueUpdate.process(Update);



	} catch (error) {
		console.log(error)
	}
}

module.exports = {
	// Para elegir cual es el que se quiera inicializar mas veces
	View,Create, Delete, FindOne, Update,
	// Para inicializar todo el modulo completo
	run
}