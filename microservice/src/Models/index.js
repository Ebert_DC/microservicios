const { sequelize } = require('../settings')
const { DataTypes } = require('sequelize');

// se define el modelo donde se trabajara
// tabla se llama 'curso'
const Model = sequelize.define('curso', {
	// tiene ciertos atributos
	// el ID lo toma de forma automatica
	// Con esto se optiene una Base de datos Seteada
	name: { type: DataTypes.STRING },
	age: { type: DataTypes.BIGINT },
	color: { type: DataTypes.STRING },

});

// Tenemos que crear una Inicialización mediante una funcicon
const SyncDB = async () => {
	try {
		
		console.log("Vamos a inicializar la DB");

		await Model.sync({ logging: false});
		
		console.log("Base de datos inicializada");


		return { statusCode: 200, data: 'ok'}
		
	} catch (error) {
		console.log(error);

		return { statusCode: 500, message: error.toString() }

	}
}

module.exports = { SyncDB, Model }