// Alguien que procesa
const Controllers = require('../Controllers');
const { InternalError } = require('../settings');

const Create = async ({ age, color, name }) => {

	try {
		
		let { statusCode, data, menssage } = await Controllers.Create({ age, color, name })
		
		return { statusCode, data, menssage }
		

	} catch (error) {
		console.log({ step: 'service Create', error: error.toString() });
		return { statusCode: 500, message: error.toString() };
	}
	
};

const Delete = async ({ id }) => {

	try {

		const findOne = await Controllers.FindOne({ where: { id } });

		if ( findOne.statusCode !== 200 ) {
			/**
			let response = {
				400: { statusCode: 400, menssage: "No existe el usuario a eliminar"},
				500: { statusCode: 500, menssage: InternalError}
			}

			return response [findOne.statusCode]
			 */
			
			// Se utiliza en caso de que no sepamos que Errores provengan
			switch (findOne.statusCode) {
				case 400: return { statusCode: 400, menssage: "No existe el usuario a eliminar"}
				
				case 500: return { statusCode: 500, menssage: InternalError}
													
				default: return { statusCode: findOne.statusCode, menssage: findOne.menssage }
				 
			}
		};
		
		const del  = await Controllers.Delete({ where: { id } });

		if (del.statusCode === 200 ) return { statusCode: 200, data: findOne.data }

		return {statusCode: 400, message: InternalError}

	} catch (error) {
		console.log({ step: 'service Delete', error: error.toString() });
		return { statusCode: 500, message: error.toString() };
	}
	
};

const Update = async ({ age, color, name, id }) => {

	try {
		
		let { statusCode, data, message } = await Controllers.Update({ age, color, name, id })
		
		return { statusCode, data, message };
		

	} catch (error) {
		console.log({ step: 'service Update', error: error.toString() });
		return { statusCode: 500, message: error.toString() };
	}
	
};

const FindOne = async ({ id }) => {

	try {
		
		let { statusCode, data, menssage } = await Controllers.FindOne({ where: { id} })
		
		return { statusCode, data, menssage }
		

	} catch (error) {
		console.log({ step: 'service FindOne', error: error.toString() });
		return { statusCode: 500, message: error.toString() };
	}
	
};

const View = async ({ }) => {

	try {
		// Por ahora no lleva parametros
		let { statusCode, data, menssage } = await Controllers.View({ where:{} })
		
		return { statusCode, data, menssage }
		

	} catch (error) {
		console.log({ step: 'service View', error: error.toString() });
		return { statusCode: 500, message: error.toString() };
	}
	
};

module.exports = { Create, Delete, Update, FindOne, View }