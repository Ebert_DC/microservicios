// Importar base de datos
const { Model } = require('../Models');

// CRUD	, Son todas las opcones que se puede hacer con este controlador
// Leer la documentacion de la DB
// Se utilisa " logging: false " para ocultar los LOGS de la DB
// Primero son los valores
const Create = async ({ name, age, color }) => {
	try {
		// Estos serian los parametros  name, age, color 
		let instance = await Model.create(
			{ name, age, color },
			{ fields: [ 'name', 'age', 'color' ], logging: false}
		);

		return { statusCode: 200, data: instance.toJSON() }
		
	} catch (error) {
		console.log({ step: 'controller Create', error: error.toString() })

		return { statusCode: 400, menssage: error.toString() }
	}
};

const Delete = async ({ where: { id } }) => {
	try {
		
		// Estos serian los parametros  name
		// Si no se puede el where se borra toda la db
		await Model.destroy({ where: { id } , logging: false });

		return { statusCode: 200, data: "OK" }

	} catch (error) {
		console.log({ step: 'controller Delete', error: error.toString() });

		return { statusCode: 400, menssage: error.toString() };

	}
};

const Update = async ({ name, age, color, id }) => {
	try {

		// Estos serian los parametros  name, age, color 
		let instance = await Model.update(
			{ name, age, color },
			{ where: { id }, logging: false, returning: true }
		);

		// Se optendria la unica instancia que se a actualizado
		return { statusCode: 200, data: instance[1][0].toJSON() }		
		
	} catch (error) {
		console.log({ step: 'controller Update', error: error.toString() });

		return { statusCode: 400, message: error.toString() };

	}
};

const FindOne = async ({where: { id } }) => {
	try {

		let instance = await Model.findOne({ where: id , logging: false })
		
		if (instance)return { statusCode: 200, data: instance.toJSON() };

		else return { statusCode: 400, menssage: "No existe el usuario" };

	} catch (error) {
		console.log({ step: 'controller FindOne', error: error.toString() });

		return { statusCode: 400, menssage: error.toString() };

	}
};

const View = async ({ where: { }}) => {
	try {

		let instance = await Model.findAll({ where: {}, logging: false });

		return { statusCode: 200, data: instance }

		
	} catch (error) {
		console.log({ step: 'controller View', error: error.toString() });

		return { statusCode: 400, menssage: error.toString() };

	}
};


module.exports = { Create, Delete, Update, FindOne, View }