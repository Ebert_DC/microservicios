const { io } = require('socket.io-client');

// Se pasa el parametro de la url donde se conectara 192.168.0.16 -> , { transports: ['websocket'] }
const socket = io("http://localhost")

async function main () {
    /**
     * res. es para enviar 
     * req. para enviar parametros
     */
    try {
        setTimeout(() => console.log(socket.id), 500);
        // Escucho la informacion que recivo
        socket.on('res:microservice:view', ({ statusCode, data, message }) => {
            console.log('res:microservice:view',{ statusCode, data, message })
        });

        socket.on('res:microservice:create', ({ statusCode, data, message }) => {
            console.log('res:microservice:create',{ statusCode, data, message })
        });

        socket.on('res:microservice:findOne', ({ statusCode, data, message }) => {
            console.log('res:microservice:findOne',{ statusCode, data, message })
        });

        socket.on('res:microservice:update', ({ statusCode, data, message }) => {
            console.log('res:microservice:update',{ statusCode, data, message })
        });

        socket.on('res:microservice:delete', ({ statusCode, data, message }) => {
            console.log('res:microservice:delete',{ statusCode, data, message })
        });

        // Envio la informacion
        //setInterval(() => socket.emit('req:microservice:view', ({})), 50);

        setTimeout(() => {
            //socket.emit('req:microservice:create', ({ age: 10, color: 'Blue', name: 'Dani'}));

            //socket.emit('req:microservice:update', ({ age: 500, color: 'Blue', name: 'Dani', id: 8}));

            //socket.emit('req:microservice:delete', ({ id: 6}));

            socket.emit('req:microservice:findOne', ({ id: 1}));

            //socket.emit('req:microservice:view', ({}));

        }, 300);

    } catch (error) {
        console.log(error);
    }
}

main()