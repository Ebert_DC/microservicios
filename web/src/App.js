import './App.css'
import React ,{ useEffect, useState } from 'react'

const { io } = require('socket.io-client');

const socket = io("http://localhost",{ transports: ['websocket'],jsonp: false });

function App() {
  const [id, setId] = useState();
  
  const [data, setData] = useState([]);

  

  useEffect(() =>{
    

    setTimeout(() => setId(socket.id), 500);
      // Escucho la informacion que recivo
    socket.on('res:microservice:view', ({ statusCode, data, message }) => {
        
        // console.log('res:microservice:view',{ statusCode, data, message });

        // console.log({ statusCode, data, message });

        if(statusCode === 200) setData(data)


    });

    socket.on('res:microservice:create', ({ statusCode, data, message }) => {
        console.log('res:microservice:create',{ statusCode, data, message })
    });

    socket.on('res:microservice:findOne', ({ statusCode, data, message }) => {
        console.log('res:microservice:findOne',{ statusCode, data, message })
    });

    socket.on('res:microservice:update', ({ statusCode, data, message }) => {
        console.log('res:microservice:update',{ statusCode, data, message })
    });

    socket.on('res:microservice:delete', ({ statusCode, data, message }) => {
        console.log('res:microservice:delete',{ statusCode, data, message })
    });

  // Envio la informacion
  socket.emit('req:microservice:view', ({}));
  
  }, []);
/** Formulario */
  const [datos, setDatos] = useState({
    nombre: '',
    edad: 0,
    color: ''
  });

  const handleInputChange = (event) => {
    setDatos({
      ...datos,
      [event.target.name] : event.target.value
    });
  }

  const enviarDatos = (event) => {
    event.preventDefault();
    
    socket.emit('req:microservice:create', ({ age: datos.edad, color: datos.color, name: datos.nombre }));

    //console.log('enviando datos...' + datos.nombre + ' ' + parseInt(datos.edad) + ' ' + datos.color)
    //console.log(parseInt(datos.edad))

  }

  return (
    <div>
      <p>{id ? `Estas en linea ${id}` : 'Fuera de linea'} </p>
      <br/>

      <h1>Formulario</h1>
      <form className="row" onSubmit={enviarDatos}>
          <div className="col-md-3">
              <input type="text" placeholder="Nombre" className="form-control" onChange={handleInputChange} name="nombre"></input>
          </div>
          <div className="col-md-3">
              <input type="number" placeholder="Edad" className="form-control" onChange={handleInputChange} name="edad"></input>
          </div>
          <div className="col-md-3">
              <input type="text" placeholder="Color" className="form-control" onChange={handleInputChange} name="color"></input>
          </div>
          <button type="submit" className="btn btn-primary">Enviar</button>
      </form>


      <br/>
      <table>
        <thead>
        <tr>
          <th>Nombre</th>
          <th>Edad</th>
          <th>Color</th>
          <th>Worker</th>
        </tr>
        </thead>
        <tbody>
          { data.map((v, i) => <tr key={i} >
            <td>{v.name}</td>
            <td> {v.age}</td>
            <td> {v.color}</td>
            <td> {v.worker}</td>
          </tr> )}
        </tbody>
      </table>
    </div>
  );
}

export default App;
