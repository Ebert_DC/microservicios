// Alguien que adapta al mundo exterior
//Los adaptadores no pueden tener mas de un servicio
const Services = require('../Services');
const { InternalError } = require('../settings');

const { queueView, queueCreate, queueDelete, queueFindOne, queueUpdate } = require('./index');
// Lo extendemos para despues poder tener definir varios endpoint
async function View (job, done) {

	try {

		const {  } = job.data;

		let { statusCode, data, message} = await Services.View({ });
		
		done(null, { statusCode, data, message});
		

	} catch (error) {
		console.log({ step: 'adapter queueView', error: error.toString() });

		done(null, { statusCode: 500, message: InternalError });
	}

};

async function Create (job, done) {

	try {
		// Se destructutura la data para optener el ID
		const { title, image } = job.data;

		let { statusCode, data, message} = await Services.Create({ title, image });
		
		done(null, { statusCode, data, message});
		

	} catch (error) {
		console.log({ step: 'adapter queueCreate', error: error.toString() });

		done(null, { statusCode: 500, message: InternalError });
	}

};

async function Delete (job, done) {

	try {
		// Se destructutura la data para optener el ID
		const { id } = job.data;

		let { statusCode, data, message} = await Services.Delete({ id });
		
		done(null, { statusCode, data, message});
		

	} catch (error) {
		console.log({ step: 'adapter queueDelete', error: error.toString() });

		done(null, { statusCode: 500, message: InternalError });
	}

};

async function FindOne (job, done) {

	try {
		// Se destructutura la data para optener el ID
		const { title } = job.data;

		let { statusCode, data, message} = await Services.FindOne({ title });
		
		done(null, { statusCode, data, message});
		

	} catch (error) {
		console.log({ step: 'adapter queueFindOne', error: error.toString() });

		done(null, { statusCode: 500, message: InternalError });
	}

};

async function Update (job, done) {

	try {
		// Se destructutura la data para optener el ID
		const { id, category, seccions, title  } = job.data;

		let { statusCode, data, message} = await Services.Update({ id, category, seccions, title });
		
		done(null, { statusCode, data, message});
		

	} catch (error) {
		console.log({ step: 'adapter queueUpdate', error: error.toString() });

		done(null, { statusCode: 500, message: InternalError });
	}

};

async function run() {
	try {
		// setTimeout(async () => {
		// 	console.log("Creando prueba");
		// 	const job = await queueCreate.add({
		// 		image: "https://www.google.com/url?sa=i&url=https%3A%2F%2Fdevcode.la%2Ftutoriales%2Fmiddlewares-en-nodejs%2F&psig=AOvVaw0nXMszI_UfqsBQLW5utrWp&ust=1647469320504000&source=images&cd=vfe&ved=0CAsQjRxqFwoTCLCRv9CTyfYCFQAAAAAdAAAAABAD",
		// 		title: "Aprende nodejs"

		// 	})
		// 	const { statusCode, data, message } = await job.finished();

		// 	console.log({ statusCode, data, message })
		// }, 2000);
		
		console.log("Vamos a inicializar la Worker");

		queueView.process(View);
		queueCreate.process(Create);
		queueDelete.process(Delete);
		queueFindOne.process(FindOne);
		queueUpdate.process(Update);



	} catch (error) {
		console.log(error)
	}
}

module.exports = {
	// Para elegir cual es el que se quiera inicializar mas veces
	View,Create, Delete, FindOne, Update,
	// Para inicializar todo el modulo completo
	run
}