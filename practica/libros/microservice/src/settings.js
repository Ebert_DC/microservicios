const dotenv = require('dotenv');
// Para la base de datos
const { Sequelize } = require('sequelize');

const { name } = require('../package.json')

dotenv.config();

const redis = {
	host: process.env.REDIS_HOST,
	port: process.env.REDIS_PORT,
};

const InternalError = 'No podemos procesar tu solicitud en este momento';
// Se crea una instacia
const sequelize = new Sequelize({

	host: process.env.POSTGRES_HOST,
	port: process.env.POSTGRES_PORT,
	database: process.env.POSTGRES_DB,
	username: process.env.POSTGRES_USER,
	password: process.env.POSTGRES_PASSWORD,
	dialect: 'postgres',
})

module.exports = { redis, InternalError, sequelize, name };