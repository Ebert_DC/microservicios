//primero se importa la funcion de syc del modelo y exportarla
const { SyncDB } = require('./Models');
const { run } = require('./Adapters/processor');

module.exports = { SyncDB, run }
