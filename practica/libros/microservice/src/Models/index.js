const { sequelize } = require('../settings')
const { DataTypes } = require('sequelize');

// se define el modelo donde se trabajara
// tabla se llama 'curso'
const Model = sequelize.define('libros', {
	
	title: { type: DataTypes.STRING },

	category: { type: DataTypes.BIGINT },

	seccions: { type: DataTypes.STRING },

	image: { type: DataTypes.STRING },

});

// Tenemos que crear una Inicialización mediante una funcicon
const SyncDB = async () => {
	try {
		
		await Model.sync({ logging: false, force: true});
		
		return { statusCode: 200, data: 'ok'}
		
	} catch (error) {
		console.log(error);

		return { statusCode: 500, message: error.toString() }

	}
}

module.exports = { SyncDB, Model }