const api = require('./api');

async function main() {
    try {
        let { data, statusCode, message } = await api.Create({
            image: "https://nodejs.org/static/images/logo.svg",
            title: "Aprende nodejs v2"
        })
        //let { data, statusCode, message } = await api.Delete({id: 1})
        console.log({ data, statusCode, message})
    } catch (error) {
        console.log(error)
    }
}

main();