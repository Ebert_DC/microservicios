import React from 'react'
import './App.css'
import styled from 'styled-components'

import Socios from './Socios'
import Libros from './Libros'
import Pagos from './Pagos'

const Container = styled.div`
  display: flex;
  flex-direction: row;
  min-height: calc(100vh - 4em);
  
`

const Slider = styled.div`
  width: 35%;
  min-width: 350px;
`

const Body = styled.div`
  width: 65%;
  min-width: 350px;
  
`

function App() {

  return (
    <div className="App">
      <Container>

        <Slider>

          <Socios/>

          <Pagos/>

        </Slider>
        <Body>

          <Libros/>

        </Body>
        
      </Container>
    </div>
  )
}

export default App
