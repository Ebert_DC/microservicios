import React,{ useEffect, useState} from "react"
import styled from "styled-components"

import { socket } from "./ws"

const Container = styled.div`

    position: relative;
    flex-direction: row;
    flex-wrap: wrap;
    justify-content: space-evenly;
    align-self: center;
    align-content: stretch;
    display: flex;
    width: 100%; 

`
const Libro = styled.div`
    background-color: #303536;
    margin: 15px 5px 0px;

    border-radius: 10px;
    padding: 2px;
    flex-direction: column;
    position: relative;

`
const Portada = styled.img`
    width: 200px;
`
const Icon = styled.img`
    height: 35px;
    width: 35px;
    position: absolute;
    top: -10px;
    right: -10px;
    cursor: pointer;
`
const Title = styled.p`
    color: white;
    text-align: center;
`
const Button = styled.button`
    background-color: #036e00;
    padding-left: 20px;
    padding-right: 20px;
    padding-top: 10px;
    padding-bottom: 10px;
    opacity: 0.8;
    border-radius: 15px;
    color: white;
    width: 90%;
`
const Item = ({ image, title, id}) => {
    const handleDelete = () => { socket.emit('req:libros:delete', { id })}

    const opts = {
        src: "https://cdn-icons-png.flaticon.com/512/1828/1828843.png",
        onClick: handleDelete
    }

    return (
        <Libro>
            <Icon {...opts}/>
            <Portada src={image}/>
            <Title>{title}</Title>
        </Libro>
    )
}

const App = () => {

    const [data, setData] = useState([]);

    useEffect(() => {
        socket.on('res:libros:view', ({ statusCode, data, message }) => {

            console.log('res:libros:view', { statusCode, data, message });

            console.log({ statusCode, data, message });

            if ( statusCode === 200 ) setData(data);

        });

        setTimeout(() => socket.emit('req:libros:view', ({})), 1000);
    }, []);

    const handleCreate =  () => socket.emit('req:libros:create', {
        image: "https://nodejs.org/static/images/logo.svg",
        title: "Aprende nodejs v2"
    })

    return(
        <Container>
            <Button onClick={handleCreate}>Create a Book</Button>
            { data.map((v, i) =>  <Item key={i} {...v} /> )}
        </Container>
    )
}

export default App;
