import React, { useState, useEffect } from "react";
import { socket } from "./ws";

import styled from "styled-components"

const Container = styled.div`
	margin-top: 35px;
    width: 100%;
` 
const ContainerBody = styled.div`
    width: 100%;
    margin-top: 15px;

    height: 350px;
    overflow-y: scroll;
` 

const Socio = styled.div`
    padding: 0 10px;
    display: flex;
    flex-direction: column;
    border-style: solid;
    margin-bottom: 15px;
    position: relative;

`
const Body = styled.div`
    padding-left: 15px;
    padding-right: 15;
    display: flex;
    flex-direction: row;
    justify-content: space-between;
    align-items: center;
`
const Name = styled.p`
    color: #333;
`

const Button = styled.button`
	margin-top: 15px;
    background-color: #f44336;
    padding-left: 20px;
    padding-right: 20px;
    padding-top: 10px;
    padding-bottom: 10px;
    opacity: 0.8;
    border-radius: 15px;
    color: white;
    width: 90%;
`
const Icon = styled.img`
    margin-left: 10px;
    height: 25px;
    width: 25px;
    cursor: pointer;
`
const Input = styled.input`
    margin-left: 10px;
    width: 90%;
    height: 25px;
	border-radius: 5px;
    cursor: pointer;
`

const App = () => {

	const [data, setData] = useState([]);
	const [value, setValue] = useState();

    useEffect(() => {
        socket.on('res:pagos:view', ({ statusCode, data, message }) => {

            console.log('res:pagos:view', { statusCode, data, message });

            console.log({ statusCode, data, message });

            if ( statusCode === 200 ) setData(data);

        });

        setTimeout(() => socket.emit('req:pagos:view', ({ })), 1000);
    }, []);

    const handleCreate =  () => {
		value > 0 ? socket.emit('req:pagos:create', {socio: value, amount: 300 }) : null
	}

    const handleDelete = (id) => { socket.emit('req:pagos:delete', { id })}   

	const handleInput = (e) => setValue(e.target.value);

	return (
		<Container>

			<Input type={'number'} onChange={handleInput}></Input>

			<Button onClick={handleCreate} >Create a Payment for {value}</Button>
			<ContainerBody>
				{
					data.map((v, i) =>( 
						<Socio key={i}>
							<Body>
								<Name> Cupon de pago {v.id} </Name>

								<Name> Socio: {v.socio} </Name>
							</Body>
							<Body>
								<Name> total: $ {v.amount}</Name>
 								
								<Icon 
									src ={"https://cdn-icons-png.flaticon.com/512/1828/1828843.png"}
									onClick={() => handleDelete(v.id)}
								/>
							</Body>
						</Socio>
					))
				}
			</ContainerBody>
		</Container>
	)
}

export default App