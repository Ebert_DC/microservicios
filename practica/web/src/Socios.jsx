import React, { useState, useEffect } from "react";
import { socket } from "./ws";

import styled from "styled-components"

const Container = styled.div`

    width: 100%;
` 
const ContainerBody = styled.div`
    width: 100%;
    margin-top: 15px;

    height: 350px;
    overflow-y: scroll;
` 

const Socio = styled.div`
    padding: 0 10px;
    display: flex;
    flex-direction: column;
    border-style: solid;
    margin-bottom: 15px;
    position: relative;

`
const Body = styled.div`
    padding-left: 15px;
    padding-right: 15;
    display: flex;
    flex-direction: row;
    justify-content: space-between;
    align-items: center;
`
const Name = styled.p`
    color: #333;
`
const Phone = styled.p`

`
const Email = styled.p`

`
const Enable = styled.div`
    width: 25px;
    height: 25px;
    border-radius: 50%;
    
    background-color: ${props => props.enable ? 'green' : 'red'};
    opacity: 0.5;
`
const Button = styled.button`
    background-color: #f44336;
    padding-left: 20px;
    padding-right: 20px;
    padding-top: 10px;
    padding-bottom: 10px;
    opacity: 0.8;
    border-radius: 15px;
    color: white;
    width: 90%;
`
const Icon = styled.img`
    margin-left: 10px;
    height: 25px;
    width: 25px;
    cursor: pointer;
`
const FeedBack = styled.div`
    display: flex;
    flex-direction: row;
    align-items: center;
`
const App = () => {

    const [data, setData] = useState([]);

    useEffect(() => {
        socket.on('res:socios:view', ({ statusCode, data, message }) => {

            console.log('res:socios:view', { statusCode, data, message });

            console.log({ statusCode, data, message });

            if ( statusCode === 200 ) setData(data);

        });

        setTimeout(() => socket.emit('req:socios:view', ({ })), 1000);
    }, []);

    const handleCreate =  () => socket.emit('req:socios:create', {
        name: 'Fany', phone: 1945454
    })

    const handleDelete = (id) => { socket.emit('req:socios:delete', { id })}   

    const handleChangeEstatus = (id, status) => {
        if(status) socket.emit('req:socios:disable', {id }) 

        else socket.emit('req:socios:enable', {id })
    }

    return(
        <Container>
            <Button onClick={handleCreate} >Create a Partner</Button>
            <ContainerBody>
                {
                    data.map((v, i) =>( 
                        <Socio key={i}>
                            <Body>
                                <Name> {v.name} <small>{v.id}</small> </Name>
                                <Phone> {v.phone} </Phone>
                            </Body>
                            <Body>
                                <Email> {v.email}</Email>
                                
                                <FeedBack>
                                    <Enable enable={v.enable} onClick={()=> handleChangeEstatus(v.id, v.enable)} />

                                    <Icon 
                                        src ={"https://cdn-icons-png.flaticon.com/512/1828/1828843.png"}
                                        onClick={() => handleDelete(v.id)}
                                    />
                                </FeedBack>
                            </Body>
                        </Socio>
                    ))
                }
            </ContainerBody>
        </Container>
    );
}

export default App;