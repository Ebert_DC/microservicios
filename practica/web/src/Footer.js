import React, {useEffect, useState} from "react";

const App = () => {
    const [id, setId] = useState();

    useEffect(() => setTimeout(() => setId( socket.id), 500 ),[]);

    return(
        <p>{id ? `Estas en linea ${id}`: `Fuera de linea`}</p>
    )    
}

export default App;