const express = require('express');
const http = require('http');

const app = express();

// Se crea el servidor de expres
const server = http.createServer(app);

// Se crea la conexion de socket
const { Server, Socket } = require('socket.io');

// Se crea una constante io 
const io = new Server(server);

// Importacion de la api
const apiLibros = require('api-libros');
const apiSocio = require('api-socio');
const apiPagos = require('api-pagos');

// Se inicializa el Servidor 
server.listen(80, () => {
    console.log("Server Initialize");

    io.on('connection', socket => {
        console.log("New connection  ", socket.id);

        // Libros

        socket.on('req:libros:view', async ({ }) => {
           
            try {
                
                console.log('req:libros:view');
    
                const { statusCode, data, message } = await apiLibros.View({});
                // Se emit solo al socket que hizo la consulta
                return io.to(socket.id).emit('res:libros:view', { statusCode, data, message });

            } catch (error) {
                console.log(error);
            }

        });

        socket.on('req:libros:create', async ({ title, image }) => {
           
            try {
                
                console.log('req:libros:create', ({ title, image }));
                    
                const { statusCode, data, message } = await apiLibros.Create({ title, image});
                // Se emit solo al socket que hizo la consulta
                io.to(socket.id).emit('res:libros:create', { statusCode, data, message });

                const view = await apiLibros.View({})

                //Se envia los datos al id
                return io.to(socket.id).emit('res:libros:view', view );

            } catch (error) {
                console.log(error);
            }
            
        });

        socket.on('req:libros:findOne', async ({ title }) => {
           
            try {
                
                console.log('req:libros:findOne', ({ title }));
                                    
                const { statusCode, data, message } = await apiLibros.FindOne({ title });
                // Se emit solo al socket que hizo la consulta
                return io.to(socket.id).emit('res:libros:findOne', { statusCode, data, message });

            } catch (error) {
                console.log(error);
            }

        });

        socket.on('req:libros:update', async ({ id, category, seccions, title }) => {
           
            try {
                
                console.log('req:libros:update', ({ id, category, seccions, title }));
                    
                const { statusCode, data, message } = await apiLibros.Update({ id, category, seccions, title });
                // Se emit solo al socket que hizo la consulta
                return io.to(socket.id).emit('res:libros:update', { statusCode, data, message, id });

            } catch (error) {
                console.log(error);
            }

        });

        socket.on('req:libros:delete', async ({ id }) => {
           
            try {
                
                console.log('req:libros:delete', ({ id }));
                                    
                const { statusCode, data, message } = await apiLibros.Delete({ id });
                
                // Se emit solo al socket que hizo la consulta
                io.to(socket.id).emit('res:libros:delete', {statusCode, data, message })

                // Despues de eliminar se actia el view para que actualice los datos
                const view = await apiLibros.View({})

                //Se envia los datos al id
                return io.to(socket.id).emit('res:libros:view', view );

            } catch (error) {
                console.log(error);
            }

        });

        // Pagos
        
        socket.on('req:pagos:view', async ({ }) => {
           
            try {
                
                console.log('req:pagos:view');
    
                const { statusCode, data, message } = await apiPagos.View({});
                // Se emit solo al socket que hizo la consulta
                return io.to(socket.id).emit('res:pagos:view', { statusCode, data, message });

            } catch (error) {
                console.log(error);
            }

        });

        socket.on('req:pagos:create', async ({ socio, amount }) => {
           
            try {
                
                console.log('req:pagos:create', ({ socio, amount }));
                    
                const { statusCode, data, message } = await apiPagos.Create({ socio, amount });
                // Se emit solo al socket que hizo la consulta
                io.to(socket.id).emit('res:pagos:create', { statusCode, data, message });

                const view = await apiPagos.View({});
                // Se emit solo al socket que hizo la consulta
                return io.to(socket.id).emit('res:pagos:view', view);

            } catch (error) {
                console.log(error);
            }
            
        });

        socket.on('req:pagos:findOne', async ({ id }) => {
           
            try {
                
                console.log('req:pagos:findOne', ({ id }));
                                    
                const { statusCode, data, message } = await apiPagos.FindOne({ id });
                // Se emit solo al socket que hizo la consulta
                return io.to(socket.id).emit('res:pagos:findOne', { statusCode, data, message });

            } catch (error) {
                console.log(error);
            }

        });

        socket.on('req:pagos:delete', async ({ id }) => {
           
            try {
                
                console.log('req:pagos:delete', ({ id }));
                                    
                const { statusCode, data, message } = await apiPagos.Delete({ id });
                // Se emit solo al socket que hizo la consulta
                io.to(socket.id).emit('res:pagos:delete', { statusCode, data, message });
                
                const view = await apiPagos.View({});
                // Se emit solo al socket que hizo la consulta
                return io.to(socket.id).emit('res:pagos:view', view);

            } catch (error) {
                console.log(error);
            }

        });

        // Socios
        
        socket.on('req:socios:view', async ({ enable }) => {
           
            try {
                
                console.log('req:socios:view', ({ enable }));
    
                const { statusCode, data, message } = await apiSocio.View({enable});
                // Se emit solo al socket que hizo la consulta
                return io.to(socket.id).emit('res:socios:view', { statusCode, data, message });

            } catch (error) {
                console.log(error);
            }

        });

        socket.on('req:socios:create', async ({ name, phone }) => {
           
            try {
                
                console.log('req:socios:create', ({ name, phone }));
                    
                const { statusCode, data, message } = await apiSocio.Create({ name, phone});
                // Se emit solo al socket que hizo la consulta
                io.to(socket.id).emit('res:socios:create', { statusCode, data, message });
                
                const view = await apiSocio.View({ })

                //Se envia los datos al id
                return io.to(socket.id).emit('res:socios:view', view );

            } catch (error) {
                console.log(error);
            }
            
        });

        socket.on('req:socios:findOne', async ({ id }) => {
           
            try {
                
                console.log('req:socios:findOne', ({ id }));
                                    
                const { statusCode, data, message } = await apiSocio.FindOne({ id });
                // Se emit solo al socket que hizo la consulta
                return io.to(socket.id).emit('res:socios:findOne', { statusCode, data, message });

            } catch (error) {
                console.log(error);
            }

        });

        socket.on('req:socios:update', async ({ id, name, age, phone, email }) => {
           
            try {
                
                console.log('req:socios:update', ({ id, name, age, phone, email }));
                    
                const { statusCode, data, message } = await apiSocio.Update({ id, name, age, phone, email });
                // Se emit solo al socket que hizo la consulta
                return io.to(socket.id).emit('res:socios:update', { statusCode, data, message, id });

            } catch (error) {
                console.log(error);
            }

        });

        socket.on('req:socios:delete', async ({ id }) => {
           
            try {
                
                console.log('req:socios:delete', ({ id }));
                                    
                const { statusCode, data, message } = await apiSocio.Delete({ id });
                // Se emit solo al socket que hizo la consulta
                io.to(socket.id).emit('res:socios:delete', { statusCode, data, message });

                const view = await apiSocio.View({ })

                //Se envia los datos al id
                return io.to(socket.id).emit('res:socios:view', view );

            } catch (error) {
                console.log(error);
            }

        });

        socket.on('req:socios:enable', async ({ id }) => {
           
            try {
                
                console.log('req:socios:enable', ({ id }));
                                    
                const { statusCode, data, message } = await apiSocio.Enable({ id });
                // Se emit solo al socket que hizo la consulta
                io.to(socket.id).emit('res:socios:enable', { statusCode, data, message });
                
                const view = await apiSocio.View({ })

                //Se envia los datos al id
                return io.to(socket.id).emit('res:socios:view', view );

            } catch (error) {
                console.log(error);
            }

        });

        socket.on('req:socios:disable', async ({ id }) => {
           
            try {
                
                console.log('req:socios:disable', ({ id }));
                                    
                const { statusCode, data, message } = await apiSocio.Disable({ id });
                // Se emit solo al socket que hizo la consulta
                io.to(socket.id).emit('res:socios:disable', { statusCode, data, message });

                const view = await apiSocio.View({ })

                //Se envia los datos al id
                return io.to(socket.id).emit('res:socios:view', view );

            } catch (error) {
                console.log(error);
            }

        });
    });
});