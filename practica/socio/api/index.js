const bull = require('bull');
const { name } = require('./package.json')


const redis = { host: '192.168.0.16', port: '6379' };

const options = { redis: { host: redis.host, port: redis.port } }

// Con esto queda el codigo listo con las varibles definidas para poder procesarlo
const queueCreate = bull(`${name.replace('api-','')}:create`, options );

const queueDelete = bull(`${name.replace('api-','')}:delete`, options );

const queueUpdate = bull(`${name.replace('api-','')}:update`, options );

const queueFindOne = bull(`${name.replace('api-','')}:findOne`, options );

const queueView = bull(`${name.replace('api-','')}:view`, options );

const queueEnable = bull(`${name.replace('api-','')}:enable`, options );

const queueDisable = bull(`${name.replace('api-','')}:disable`, options );

const Enable = async ({ id }) => {

	try {
		
		// Solicitar nueva tarea
		const job = await queueEnable.add({ id });

		// para trabajois con otro microservicios se tiene 	que modificare el result y poner { statusCode, data, message }
		const { statusCode, data, message } = await job.finished()

		return { statusCode, data, message };
	} catch (error) {
		console.log(error);
	}	
	
};

const Disable = async ({ id }) => {

	try {
		
		// Solicitar nueva tarea
		const job = await queueDisable.add({ id });

		// para trabajois con otro microservicios se tiene 	que modificare el result y poner { statusCode, data, message }
		const { statusCode, data, message } = await job.finished()

		return { statusCode, data, message };
	} catch (error) {
		console.log(error);
	}	
	
};


const Create = async ({ name, phone }) => {

	try {
		
		// Solicitar nueva tarea
		const job = await queueCreate.add({ name, phone });

		// para trabajois con otro microservicios se tiene 	que modificare el result y poner { statusCode, data, message }
		const { statusCode, data, message } = await job.finished()

		return { statusCode, data, message };
	} catch (error) {
		console.log(error);
	}	
	
};

const Delete = async ({ id }) => {

	try {
		
		// Solicitar nueva tarea
		const job = await queueDelete.add({ id })
		
		const { statusCode, data, message } = await job.finished()
		
		return { statusCode, data, message};
	
	} catch (error) {
		console.log(error);
	}	
	
};

const FindOne = async ({ id }) => {

	try {
		
		// Solicitar nueva tarea
		const job = await queueFindOne.add({ id })
		
		const { statusCode, data, message } = await job.finished()
		
		return { statusCode, data, message};

	} catch (error) {
		console.log(error);
	}	
	
};

const Update = async ({ id, name, age, phone, email }) => {

	try {

		// Solicitar nueva tarea
		const job = await queueUpdate.add({ id, name, age, phone, email })

		const { statusCode, data, message } = await job.finished()
		
		return { statusCode, data, message};
	
	} catch (error) {
		console.log(error);
	}
		
};

const View = async ({ enable }) => {

	try {

		// Solicitar nueva tarea
		const job = await queueView.add({ enable })

		const { statusCode, data, message } = await job.finished()
		
		return { statusCode, data, message};
	} catch (error) {
		console.log(error);
	}

		
	
};

const main = async () => {

	//await Create({name: 'Alberto', color: 'Negro', age: 20});
	
	//await Delete({ id: 4 })

	//await Update({ name: "AlbertoED", id: 7 })

	//await FindOne({ id: 2 });

	//await View({});
};

// main();

module.exports = { Create, Delete, Update, FindOne, View, Enable, Disable }
