// Alguien que procesa
const Controllers = require('../Controllers');
const { InternalError } = require('../settings');


const Enable = async ({ id }) => {

	try {
		
		let { statusCode, data, menssage } = await Controllers.Enable({ id })
		
		return { statusCode, data, menssage }
		

	} catch (error) {
		console.log({ step: 'service Enable', error: error.toString() });
		return { statusCode: 500, message: error.toString() };
	}
	
};

const Disable = async ({ id }) => {

	try {
		
		let { statusCode, data, menssage } = await Controllers.Disable({ id })
		
		return { statusCode, data, menssage }
		

	} catch (error) {
		console.log({ step: 'service Disable', error: error.toString() });
		return { statusCode: 500, message: error.toString() };
	}
	
};

const Create = async ({ name, phone }) => {

	try {
		
		let { statusCode, data, menssage } = await Controllers.Create({ name, phone })
		
		return { statusCode, data, menssage }
		

	} catch (error) {
		console.log({ step: 'service Create', error: error.toString() });
		return { statusCode: 500, message: error.toString() };
	}
	
};

const Delete = async ({ id }) => {

	try {

		const findOne = await Controllers.FindOne({ where: { id } });

		if ( findOne.statusCode !== 200 ) {
			
			// Se utiliza en caso de que no sepamos que Errores provengan
			switch (findOne.statusCode) {
				case 400: return { statusCode: 400, menssage: "No existe el usuario a eliminar"}
								
				default: return { statusCode: 500, menssage: InternalError}
																	
			}
		};
		
		const del  = await Controllers.Delete({ where: { id } });

		if (del.statusCode === 200 ) return { statusCode: 200, data: findOne.data }

		return {statusCode: 400, message: InternalError}

	} catch (error) {
		console.log({ step: 'service Delete', error: error.toString() });
		return { statusCode: 500, message: error.toString() };
	}
	
};

const Update = async ({ id, name, age, phone, email }) => {

	try {
		
		let { statusCode, data, message } = await Controllers.Update({ id, name, age, phone, email })
		
		return { statusCode, data, message };
		

	} catch (error) {
		console.log({ step: 'service Update', error: error.toString() });
		return { statusCode: 500, message: error.toString() };
	}
	
};

const FindOne = async ({ id }) => {

	try {
		
		let { statusCode, data, menssage } = await Controllers.FindOne({ where: { id} })
		
		return { statusCode, data, menssage }
		

	} catch (error) {
		console.log({ step: 'service FindOne', error: error.toString() });
		return { statusCode: 500, message: error.toString() };
	}
	
};

const View = async ({ enable }) => {

	try {

		var where = {}

		if(enable !== undefined) where.enable = enable
		// Por ahora no lleva parametros
		let { statusCode, data, menssage } = await Controllers.View({ where })
		
		return { statusCode, data, menssage }
		

	} catch (error) {
		console.log({ step: 'service View', error: error.toString() });
		return { statusCode: 500, message: error.toString() };
	}
	
};



module.exports = { Enable, Disable, Create, Delete, Update, FindOne, View }