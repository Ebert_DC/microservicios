const { sequelize } = require('../settings')
const { DataTypes } = require('sequelize');

// se define el modelo donde se trabajara
// tabla se llama 'curso'
const Model = sequelize.define('socio', {
	
	name: { type: DataTypes.STRING },

	age: { type: DataTypes.BIGINT },

	email: { type: DataTypes.STRING },

	phone: { type: DataTypes.STRING },

	enable: { type: DataTypes.BOOLEAN, defaultValue: true}

}, { freezeTableName: true });

// Tenemos que crear una Inicialización mediante una funcicon
const SyncDB = async () => {
	try {
		
		await Model.sync({ logging: false, force: true});
		
		return { statusCode: 200, data: 'ok'}
		
	} catch (error) {

		console.log(error);

		return { statusCode: 500, message: error.toString() }

	}
}

module.exports = { SyncDB, Model }