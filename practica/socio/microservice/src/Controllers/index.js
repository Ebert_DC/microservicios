// Importar base de datos
const { Model } = require('../Models');

const Create = async ({ name, phone }) => {
	try {
		// Estos serian los parametros  name, age, color 
		let instance = await Model.create({ name, phone },{ logging: false });

		return { statusCode: 200, data: instance.toJSON() }
		
	} catch (error) {
		console.log({ step: 'controller Create', error: error.toString() })

		return { statusCode: 500, menssage: error.toString() }
	}
};

const Delete = async ({ where: { id } }) => {
	try {
		
		// Estos serian los parametros  name
		// Si no se puede el where se borra toda la db
		await Model.destroy({ where: { id } , logging: false });

		return { statusCode: 200, data: "OK" }

	} catch (error) {
		console.log({ step: 'controller Delete', error: error.toString() });

		return { statusCode: 400, menssage: error.toString() };

	}
};

const Enable = async ({ id }) => {
	try {

		let instance = await Model.update({ enable: true },{ where: { id }, logging: false, returning: true });

		// Se optendria la unica instancia que se a actualizado
		return { statusCode: 200, data: instance[1][0].toJSON() }		
		
	} catch (error) {
		console.log({ step: 'controller Enable', error: error.toString() });

		return { statusCode: 400, message: error.toString() };

	}
};

const Disable = async ({ id }) => {
	try {

		let instance = await Model.update({ enable: false },{ where: { id }, logging: false, returning: true });

		// Se optendria la unica instancia que se a actualizado
		return { statusCode: 200, data: instance[1][0].toJSON() }		
		
	} catch (error) {
		console.log({ step: 'controller Disable', error: error.toString() });

		return { statusCode: 400, message: error.toString() };

	}
};

const Update = async ({  id, name, age, phone, email  }) => {
	try {

		// Estos serian los parametros  name, age, color 
		let instance = await Model.update(
			{ name, age, phone, email },
			{ where: { id }, logging: false, returning: true }
		);

		// Se optendria la unica instancia que se a actualizado
		return { statusCode: 200, data: instance[1][0].toJSON() }		
		
	} catch (error) {
		console.log({ step: 'controller Update', error: error.toString() });

		return { statusCode: 400, message: error.toString() };

	}
};

const FindOne = async ({ id }) => {
	try {

		let instance = await Model.findOne({ where: id , logging: false })
		
		if (instance)return { statusCode: 200, data: instance.toJSON() };

		else return { statusCode: 400, menssage: "No existe el usuario" };

	} catch (error) {
		console.log({ step: 'controller FindOne', error: error.toString() });

		return { statusCode: 400, menssage: error.toString() };

	}
};

const View = async ({ where = {} }) => {
	try {

		let instance = await Model.findAll({ where, logging: false });

		return { statusCode: 200, data: instance }

		
	} catch (error) {
		console.log({ step: 'controller View', error: error.toString() });

		return { statusCode: 500, menssage: error.toString() };

	}
};


module.exports = { Create, Delete, Enable, Disable, Update, FindOne, View }