const bull = require('bull');

const { redis, name } = require('../settings');
// Se crea un objeto para eliminar las repeticiones 
const options = { redis: { host: redis.host, port: redis.port } }

// Con esto queda el codigo listo con las varibles definidas para poder procesarlo
const queueCreate = bull(`${name}:create`, options );

const queueDelete = bull(`${name}:delete`, options );

const queueUpdate = bull(`${name}:update`, options );

const queueFindOne = bull(`${name}:findOne`, options );

const queueView = bull(`${name}:view`, options );

const queueEnable = bull(`${name}:enable`, options );

const queueDisable = bull(`${name}:disable`, options );
	
module.exports = { queueCreate, queueDelete, queueUpdate, queueFindOne, queueView, queueEnable,queueDisable }