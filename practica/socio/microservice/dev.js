const micro = require('./src/index');
// Se inicializa una fuincion de inicio
async function main() {
    try {
        
        //micro.SyncDB(); // Esla que se inicializa primero, para despues de que se inicialice se procese
        const db = await micro.SyncDB();
        
        if (db.statusCode != 200) throw db.message
        
        //micro.run(); // Inicializa el proceso del microServicio
        micro.run();       


    } catch (error) {
        console.log(error)
    }
}

main()