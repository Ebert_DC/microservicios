// Inicia
const bull = require('bull');

const {name} = require('./package.json')

const redis = { host: '192.168.0.16', port: '6379' };

// Se crea un objeto para eliminar las repeticiones 
const options = { redis: { host: redis.host, port: redis.port } }

// Con esto queda el codigo listo con las varibles definidas para poder procesarlo
const queueCreate = bull(`${name.replace('api-','')}:create`, options );

const queueDelete = bull(`${name.replace('api-','')}:delete`, options );

const queueFindOne = bull(`${name.replace('api-','')}:findOne`, options );

const queueView = bull(`${name.replace('api-','')}:view`, options );

const Create = async ({ socio, amount }) => {

	try {
		
		// Solicitar nueva tarea
		const job = await queueCreate.add({ socio, amount })
		
		// para trabajois con otro microservicios se tiene 	que modificare el result y poner { statusCode, data, message }
		const { statusCode, data, message } = await job.finished()

		return { statusCode, data, message };
		
	} catch (error) {
		console.log(error);
	}
	
};

const Delete = async ({ id }) => {

	try {
		
		// Solicitar nueva tarea
		const job = await queueDelete.add({ id })
		
		const { statusCode, data, message } = await job.finished()

		return { statusCode, data, message};
	
	} catch (error) {
		console.log(error);
	}	
	
};

const FindOne = async ({ id }) => {

	try {
		
		// Solicitar nueva tarea
		const job = await queueFindOne.add({ id })
		
		const { statusCode, data, message } = await job.finished()

		return { statusCode, data, message};

	} catch (error) {
		console.log(error);
	}
	
};

const View = async ({ }) => {

	try {

		// Solicitar nueva tarea
		const job = await queueView.add({ })

		const { statusCode, data, message } = await job.finished()

		return { statusCode, data, message};

	} catch (error) {
		console.log(error);
	}

		
	
};

const main = async () => {

	//await Create({name: 'Alberto', color: 'Negro', age: 20});
	
	//await Delete({ id: 4 })

	//await Update({ name: "AlbertoED", id: 7 })

	//await FindOne({ id: 2 });

	//await View({});
};

// main();

module.exports = { Create, Delete, FindOne, View }
