// Alguien que procesa
const Controllers = require('../Controllers');
const { InternalError, redis } = require('../settings');

const socios = require('api-socio');

const Create = async ({ socio, amount }) => {

	try {

		const validarSocio = await socios.FindOne({ id: socio }, redis);
		
		console.log(validarSocio)

		// Validar al socio si existe
		if (validarSocio.statusCode !== 200 ){
			switch (validarSocio.statusCode) {
				case 400: {
					return { statusCode: 400, message: "No existe el usuario"};
				} 					
				default: return { statusCode: 400, message: InternalError};
			}
		}
		
		if (!validarSocio.data.enable){			
			return { statusCode: 400, message: "El socio no esta habilitado"};
		} 

		let { statusCode, data, message } = await Controllers.Create({ socio, amount })
		
		return { statusCode, data, message }		

	} catch (error) {
		console.log({ step: 'service Create', error: error.toString() });
		return { statusCode: 500, message: error.toString() };
	}
	
};

const Delete = async ({ id }) => {

	try {

		const findOne = await Controllers.FindOne({ where: { id } });

		if ( findOne.statusCode !== 200 ) {
			
			// Se utiliza en caso de que no sepamos que Errores provengan
			switch (findOne.statusCode) {
				case 400: return { statusCode: 400, message: "No existe el usuario a eliminar"};
				
				default: return { statusCode: 500, message: InternalError};					
				
			}
		};
		
		const del  = await Controllers.Delete({ where: { id } });

		if (del.statusCode === 200 ) return { statusCode: 200, data: findOne.data }

		return {statusCode: 400, message: InternalError}

	} catch (error) {
		console.log({ step: 'service Delete', error: error.toString() });
		return { statusCode: 500, message: error.toString() };
	}
	
};

const FindOne = async ({ id }) => {

	try {
		
		let { statusCode, data, message } = await Controllers.FindOne({ where: { id} })
		
		return { statusCode, data, message }
		

	} catch (error) {
		console.log({ step: 'service FindOne', error: error.toString() });
		return { statusCode: 500, message: error.toString() };
	}
	
};

const View = async ({ }) => {

	try {
		// Por ahora no lleva parametros
		let { statusCode, data, message } = await Controllers.View({ where:{} })
		
		return { statusCode, data, message }
		

	} catch (error) {
		console.log({ step: 'service View', error: error.toString() });
		return { statusCode: 500, message: error.toString() };
	}
	
};

module.exports = { Create, Delete, FindOne, View }