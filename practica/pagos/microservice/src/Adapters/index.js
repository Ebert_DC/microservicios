const bull = require('bull');

const { redis, name } = require('../settings');
// Se crea un objeto para eliminar las repeticiones 
const options = { redis: { host: redis.host, port: redis.port } }

// Con esto queda el codigo listo con las varibles definidas para poder procesarlo
const queueCreate = bull(`${name}:create`, options );

const queueDelete = bull(`${name}:delete`, options );

const queueFindOne = bull(`${name}:findOne`, options );

const queueView = bull(`${name}:view`, options );
	
module.exports = { queueCreate, queueDelete, queueFindOne, queueView }