// Importar base de datos
const { Model } = require('../Models');

// CRUD	, Son todas las opcones que se puede hacer con este controlador

const Create = async ({ socio, amount }) => {
	try {
		// Estos serian los parametros  name, age, color 
		let instance = await Model.create({ socio, amount },{ logging: false}
		);

		return { statusCode: 200, data: instance.toJSON() }
		
	} catch (error) {
		console.log({ step: 'controller Create', error: error.toString() })

		return { statusCode: 400, message: error.toString() }
	}
};

const Delete = async ({ where: { id } }) => {
	try {
		
		// Estos serian los parametros  name
		// Si no se puede el where se borra toda la db
		await Model.destroy({ where: { id } , logging: false });

		return { statusCode: 200, data: "OK" }

	} catch (error) {
		console.log({ step: 'controller Delete', error: error.toString() });

		return { statusCode: 400, message: error.toString() };

	}
};

const FindOne = async ({where: { id } }) => {
	try {

		let instance = await Model.findOne({ where: id , logging: false })
		
		if (instance)return { statusCode: 200, data: instance.toJSON() };

		else return { statusCode: 400, message: "No existe el usuario" };

	} catch (error) {
		console.log({ step: 'controller FindOne', error: error.toString() });

		return { statusCode: 400, message: error.toString() };

	}
};

const View = async ({ where: { }}) => {
	try {

		let instance = await Model.findAll({ where: {}, logging: false });

		return { statusCode: 200, data: instance }

		
	} catch (error) {
		console.log({ step: 'controller View', error: error.toString() });

		return { statusCode: 400, message: error.toString() };

	}
};


module.exports = { Create, Delete, FindOne, View }