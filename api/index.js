// Inicia
const bull = require('bull');

const redis = { host: '192.168.0.16', port: '6379' };
// Se crea un objeto para eliminar las repeticiones 
const options = { redis: { host: redis.host, port: redis.port } }

// Con esto queda el codigo listo con las varibles definidas para poder procesarlo
const queueCreate = bull("curso:Create", options );

const queueDelete = bull("curso:Delete", options );

const queueUpdate = bull("curso:Update", options );

const queueFindOne = bull("curso:FindOne", options );

const queueView = bull("curso:View", options );

const Create = async ({ age, color, name }) => {

	try {
		
		// Solicitar nueva tarea
		const job = await queueCreate
			// Para agregar una tarea
			.add({ age, color, name })
		// Se define un result 
		// para trabajois con otro microservicios se tiene 	que modificare el result y poner { statusCode, data, message }
		const { statusCode, data, message } = await job.finished()
	/**

		if (statusCode === 200) console.log("Hola Bienvenido ",data.name)

		else console.log(message);
	 */
		return { statusCode, data, message };
	} catch (error) {
		console.log(error);
	}

		
	
};

const Delete = async ({ id }) => {

	try {
		
		// Solicitar nueva tarea
		const job = await queueDelete.add({ id })
		
		const { statusCode, data, message } = await job.finished()
		/**
		if (statusCode === 200) console.log("Usuario eliminado es ",data.name)

		else console.log(message);
		*/
		return { statusCode, data, message};
	
	} catch (error) {
		console.log(error);
	}

		
	
};

const FindOne = async ({ id }) => {

	try {
		
		// Solicitar nueva tarea
		const job = await queueFindOne.add({ id })
		
		const { statusCode, data, message } = await job.finished()
		/**
		if (statusCode === 200) console.log("El Usuario Buscado es ",data)
		
		else console.log(message);
		*/
		return { statusCode, data, message};

	} catch (error) {
		console.log(error);
	}

		
	
};

const Update = async ({ age, color, name, id  }) => {

	try {

		// Solicitar nueva tarea
		const job = await queueUpdate.add({ age, color, name, id })

		const { statusCode, data, message } = await job.finished()
		
		return { statusCode, data, message};
	
	} catch (error) {
		console.log(error);
	}
		
};

const View = async ({ }) => {

	try {

		// Solicitar nueva tarea
		const job = await queueView.add({ })

		const { statusCode, data, message } = await job.finished()
		/**
		if (statusCode === 200) for (let x of data) console.log(x)

		else console.log( message);
		*/
		return { statusCode, data, message};
	} catch (error) {
		console.log(error);
	}

		
	
};

const main = async () => {

	//await Create({name: 'Alberto', color: 'Negro', age: 20});
	
	//await Delete({ id: 4 })

	//await Update({ name: "AlbertoED", id: 7 })

	//await FindOne({ id: 2 });

	//await View({});
};

// main();

module.exports = { Create, Delete, Update, FindOne, View }
