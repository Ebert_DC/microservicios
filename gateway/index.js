const express = require('express');
const http = require('http');

const app = express();

// Se crea el servidor de expres
const server = http.createServer(app);

// Se crea la conexion de socket
const { Server, Socket } = require('socket.io');

// Se crea una constante io 
const io = new Server(server);

// Importacion de la api
const api = require('../api');

// Se inicializa el Servidor 
server.listen(80, () => {
    console.log("Server Initialize");

    io.on('connection', socket => {
        console.log("New connection  ", socket.id);

        // Se llama al evento y se setea
        socket.on('req:microservice:view', async ({ }) => {
           
            try {
                
                console.log('req:microservice:view');
    
                const { statusCode, data, message } = await api.View({});
                // Se emit solo al socket que hizo la consulta
                return io.to(socket.id).emit('res:microservice:view', { statusCode, data, message });

            } catch (error) {
                console.log(error);
            }

        });

        socket.on('req:microservice:create', async ({ age, color, name }) => {
           
            try {
                
                console.log('req:microservice:create', ({ age, color, name }));
                    
                const { statusCode, data, message } = await api.Create({ age, color, name });
                // Se emit solo al socket que hizo la consulta
                return io.to(socket.id).emit('res:microservice:create', { statusCode, data, message });

            } catch (error) {
                console.log(error);
            }
            
        });

        socket.on('req:microservice:findOne', async ({ id }) => {
           
            try {
                
                console.log('req:microservice:findOne', ({ id }));
                                    
                const { statusCode, data, message } = await api.FindOne({ id });
                // Se emit solo al socket que hizo la consulta
                return io.to(socket.id).emit('res:microservice:findOne', { statusCode, data, message });

            } catch (error) {
                console.log(error);
            }

        });

        socket.on('req:microservice:update', async ({ age, color, name, id }) => {
           
            try {
                
                console.log('req:microservice:update', ({ age, color, name, id }));
                    
                const { statusCode, data, message } = await api.Update({ age, color, name, id });
                // Se emit solo al socket que hizo la consulta
                return io.to(socket.id).emit('res:microservice:update', { statusCode, data, message, id });

            } catch (error) {
                console.log(error);
            }

        });

        socket.on('req:microservice:delete', async ({ id }) => {
           
            try {
                
                console.log('req:microservice:delete', ({ id }));
                                    
                const { statusCode, data, message } = await api.Delete({ id });
                // Se emit solo al socket que hizo la consulta
                return io.to(socket.id).emit('res:microservice:delete', { statusCode, data, message });

            } catch (error) {
                console.log(error);
            }

        });

    });
});